<?php

use yii\db\Migration;

/**
 * Handles the creation of table `school`.
 */
class m181110_180924_create_school_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('school', [
            'school_id' => $this->primaryKey(),
            'name' => $this->string(),
            'district' => $this->string(),
            'city' => $this->string(),
            'telephone' => $this->integer(),
            'educational_stream' => $this->string(),
            'manager_name' => $this->string(),
            'manager_tel' => $this->integer(),
            'manager_email' => $this->string(),
            'address' => $this->string(),
            'administrative_manager_name' => $this->string(),
            'administrative_manager_tel' => $this->integer(),
            'administrative_manager_email' => $this->string(),
            'Accounts_manager_name' => $this->string(),
            'Accounts_manager_tel' => $this->integer(),
            'Accounts_manager_email' => $this->string(),
            
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('school');
    }
}
