<?php

use yii\db\Migration;

/**
 * Handles the creation of table `journey`.
 */
class m181117_104650_create_journey_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('journey', [
            'journey_id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'start_date' => $this->timestamp(),
            'final_date' => $this->timestamp(),
            'number_of_student_boys' => $this->integer(),
            'number_of_student_girls' => $this->integer(),
            'number_of_teachers' => $this->integer(),
            'layout' => $this->string(),
            'mixing_classes' => $this->string(),
            'preparation_day' => $this->timestamp(),
            'summary_day' => $this->timestamp(),
            'journey_manager_name' => $this->string(),
            'journey_manager_tel' => $this->integer(),
            'journey_manager_email' => $this->string(),
            'num_of_groups' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('journey');
    }
}
