<?php

use yii\db\Migration;

/**
 * Handles the creation of table `human_resources`.
 */
class m181117_095736_create_human_resources_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('human_resources', [
            'journey_id' => $this->primaryKey(),
            'educational_coordinator' => $this->string(),
            'logistics_coordinator' => $this->string(),
            'guieds' => $this->string(),
            'operational_coordinator' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('human_resources');
    }
}
