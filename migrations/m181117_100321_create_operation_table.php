<?php

use yii\db\Migration;

/**
 * Handles the creation of table `operation`.
 */
class m181117_100321_create_operation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('operation', [
            'journey_id' => $this->primaryKey(),
            'shirts' => $this->string(),
            'food_sensitivities' => $this->string(),
            'sleeping1' => $this->string(),
            'suppliers1' => $this->string(),
            'sleeping2' => $this->string(),
            'suppliers2' => $this->string(),
            'sleeping3' => $this->string(),
            'suppliers3' => $this->string(),
            'sleeping4' => $this->string(),
            'suppliers4' => $this->string(),
            'sleeping5' => $this->string(),
            'suppliers5' => $this->string(),
            'sleeping6' => $this->string(),
            'suppliers6' => $this->string(),
            'sleeping7' => $this->string(),
            'suppliers7' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('operation');
    }
}
