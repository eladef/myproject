<?php

use yii\db\Migration;

/**
 * Handles the creation of table `education`.
 */
class m181117_095327_create_education_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('education', [
            'journey_id' => $this->primaryKey(),
            'sprcial_requests' => $this->string(),
            'track_day1' => $this->string(),
            'track_day2' => $this->string(),
            'track_day3' => $this->string(),
            'track_day4' => $this->string(),
            'track_day5' => $this->string(),
            'track_day6' => $this->string(),
            'track_day7' => $this->string(),
            'more_information' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('education');
    }
}
