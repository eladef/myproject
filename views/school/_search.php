<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'district') ?>

    <?= $form->field($model, 'city') ?>

    <?= $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'educational_stream') ?>

    <?php // echo $form->field($model, 'manager_name') ?>

    <?php // echo $form->field($model, 'manager_tel') ?>

    <?php // echo $form->field($model, 'manager_email') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'administrative_manager_name') ?>

    <?php // echo $form->field($model, 'administrative_manager_tel') ?>

    <?php // echo $form->field($model, 'administrative_manager_email') ?>

    <?php // echo $form->field($model, 'Accounts_manager_name') ?>

    <?php // echo $form->field($model, 'Accounts_manager_tel') ?>

    <?php // echo $form->field($model, 'Accounts_manager_email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
