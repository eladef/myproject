<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\School */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->school_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->school_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'school_id',
            'name',
            'district',
            'city',
            'telephone',
            'educational_stream',
            'manager_name',
            'manager_tel',
            'manager_email:email',
            'address',
            'administrative_manager_name',
            'administrative_manager_tel',
            'administrative_manager_email:email',
            'Accounts_manager_name',
            'Accounts_manager_tel',
            'Accounts_manager_email:email',
        ],
    ]) ?>

</div>
