<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Journey */

$this->title = $model->journey_id;
$this->params['breadcrumbs'][] = ['label' => 'Journeys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journey-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->journey_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->journey_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'journey_id',
            'school_id',
            'start_date',
            'final_date',
            'number_of_student_boys',
            'number_of_student_girls',
            'number_of_teachers',
            'layout',
            'mixing_classes',
            'preparation_day',
            'summary_day',
            'journey_manager_name',
            'journey_manager_tel',
            'journey_manager_email:email',
            'num_of_groups',
        ],
    ]) ?>

</div>
