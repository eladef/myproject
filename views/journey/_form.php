<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\school;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeTextInput;


/* @var $this yii\web\View */
/* @var $model app\models\Journey */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="journey-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'school_id')->dropDownList(
        ArrayHelper::map(school::find()->asArray()->all(), 'school_id', 'name')
    )?>


    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'final_date')->textInput() ?>

    <?= $form->field($model, 'number_of_student_boys')->textInput() ?>

    <?= $form->field($model, 'number_of_student_girls')->textInput() ?>

    <?= $form->field($model, 'number_of_teachers')->textInput() ?>

    <?= $form->field($model, 'layout')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mixing_classes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'preparation_day')->textInput() ?>

    <?= $form->field($model, 'summary_day')->textInput() ?>

    <?= $form->field($model, 'journey_manager_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'journey_manager_tel')->textInput() ?>

    <?= $form->field($model, 'journey_manager_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_of_groups')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
