<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Journey */

$this->title = 'Update Journey: ' . $model->journey_id;
$this->params['breadcrumbs'][] = ['label' => 'Journeys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->journey_id, 'url' => ['view', 'id' => $model->journey_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="journey-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
