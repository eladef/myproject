<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JourneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Journeys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journey-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Journey', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'journey_id',
           [
            'attribute'=>'school_id',
            'value'=>'school.name',
            ],
            [
				'attribute' => 'school',
				'label' => 'schoollll',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->school->name, 
					['school/view', 'id' => $model->school->school_id]);
				}],
                
            'start_date',
            'final_date',
            'number_of_student_boys',
            //'number_of_student_girls',
            //'number_of_teachers',
            //'layout',
            //'mixing_classes',
            //'preparation_day',
            //'summary_day',
            //'journey_manager_name',
            //'journey_manager_tel',
            //'journey_manager_email:email',
            //'num_of_groups',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
