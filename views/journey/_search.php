<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JourneySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="journey-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'journey_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'start_date') ?>

    <?= $form->field($model, 'final_date') ?>

    <?= $form->field($model, 'number_of_student_boys') ?>

    <?php // echo $form->field($model, 'number_of_student_girls') ?>

    <?php // echo $form->field($model, 'number_of_teachers') ?>

    <?php // echo $form->field($model, 'layout') ?>

    <?php // echo $form->field($model, 'mixing_classes') ?>

    <?php // echo $form->field($model, 'preparation_day') ?>

    <?php // echo $form->field($model, 'summary_day') ?>

    <?php // echo $form->field($model, 'journey_manager_name') ?>

    <?php // echo $form->field($model, 'journey_manager_tel') ?>

    <?php // echo $form->field($model, 'journey_manager_email') ?>

    <?php // echo $form->field($model, 'num_of_groups') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
