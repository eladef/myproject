<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HumanResources */

$this->title = $model->journey_id;
$this->params['breadcrumbs'][] = ['label' => 'Human Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="human-resources-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->journey_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->journey_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'journey_id',
            'educational_coordinator',
            'logistics_coordinator',
            'guieds',
            'operational_coordinator',
        ],
    ]) ?>

</div>
