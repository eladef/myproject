<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HumanResources */

$this->title = 'Create Human Resources';
$this->params['breadcrumbs'][] = ['label' => 'Human Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="human-resources-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
