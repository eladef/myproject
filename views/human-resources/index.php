<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HumanResourcesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Human Resources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="human-resources-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Human Resources', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'journey_id',
            'educational_coordinator',
            'logistics_coordinator',
            'guieds',
            'operational_coordinator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
