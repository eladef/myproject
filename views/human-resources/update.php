<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HumanResources */

$this->title = 'Update Human Resources: ' . $model->journey_id;
$this->params['breadcrumbs'][] = ['label' => 'Human Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->journey_id, 'url' => ['view', 'id' => $model->journey_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="human-resources-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
