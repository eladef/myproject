<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HumanResources */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="human-resources-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'educational_coordinator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logistics_coordinator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'guieds')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operational_coordinator')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
