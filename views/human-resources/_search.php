<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HumanResourcesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="human-resources-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'journey_id') ?>

    <?= $form->field($model, 'educational_coordinator') ?>

    <?= $form->field($model, 'logistics_coordinator') ?>

    <?= $form->field($model, 'guieds') ?>

    <?= $form->field($model, 'operational_coordinator') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
