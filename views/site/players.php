<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Players;

$this->title = 'Players';
$this->params['breadcrumbs'][] = $this->title;

$query = Players::find();

  $provider = new ActiveDataProvider([
        'query' => $query,
    
    ]);
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

  <?=
    GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'id', 
            'name',
            'team',
            'age',
            'E-mail',

        ]


    ])


  ?>

    

    <code><?= __FILE__ ?></code>
</div>