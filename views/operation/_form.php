<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Operation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shirts')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'food_sensitivities')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeping7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suppliers7')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
