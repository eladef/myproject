<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Operation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'journey_id',
            'shirts',
            'food_sensitivities',
            'sleeping1',
            'suppliers1',
            //'sleeping2',
            //'suppliers2',
            //'sleeping3',
            //'suppliers3',
            //'sleeping4',
            //'suppliers4',
            //'sleeping5',
            //'suppliers5',
            //'sleeping6',
            //'suppliers6',
            //'sleeping7',
            //'suppliers7',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
