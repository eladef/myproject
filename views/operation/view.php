<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Operation */

$this->title = $model->journey_id;
$this->params['breadcrumbs'][] = ['label' => 'Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->journey_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->journey_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'journey_id',
            'shirts',
            'food_sensitivities',
            'sleeping1',
            'suppliers1',
            'sleeping2',
            'suppliers2',
            'sleeping3',
            'suppliers3',
            'sleeping4',
            'suppliers4',
            'sleeping5',
            'suppliers5',
            'sleeping6',
            'suppliers6',
            'sleeping7',
            'suppliers7',
        ],
    ]) ?>

</div>
