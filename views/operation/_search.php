<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'journey_id') ?>

    <?= $form->field($model, 'shirts') ?>

    <?= $form->field($model, 'food_sensitivities') ?>

    <?= $form->field($model, 'sleeping1') ?>

    <?= $form->field($model, 'suppliers1') ?>

    <?php // echo $form->field($model, 'sleeping2') ?>

    <?php // echo $form->field($model, 'suppliers2') ?>

    <?php // echo $form->field($model, 'sleeping3') ?>

    <?php // echo $form->field($model, 'suppliers3') ?>

    <?php // echo $form->field($model, 'sleeping4') ?>

    <?php // echo $form->field($model, 'suppliers4') ?>

    <?php // echo $form->field($model, 'sleeping5') ?>

    <?php // echo $form->field($model, 'suppliers5') ?>

    <?php // echo $form->field($model, 'sleeping6') ?>

    <?php // echo $form->field($model, 'suppliers6') ?>

    <?php // echo $form->field($model, 'sleeping7') ?>

    <?php // echo $form->field($model, 'suppliers7') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
