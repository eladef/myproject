<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EducationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="education-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'journey_id') ?>

    <?= $form->field($model, 'sprcial_requests') ?>

    <?= $form->field($model, 'track_day1') ?>

    <?= $form->field($model, 'track_day2') ?>

    <?= $form->field($model, 'track_day3') ?>

    <?php // echo $form->field($model, 'track_day4') ?>

    <?php // echo $form->field($model, 'track_day5') ?>

    <?php // echo $form->field($model, 'track_day6') ?>

    <?php // echo $form->field($model, 'track_day7') ?>

    <?php // echo $form->field($model, 'more_information') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
