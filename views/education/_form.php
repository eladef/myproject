<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\journey;

/* @var $this yii\web\View */
/* @var $model app\models\Education */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="education-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'journey_id')->dropDownList(
        ArrayHelper::map(journey::find()->asArray()->all(), 'journey_id', 'name')
    )?>

    <?= $form->field($model, 'sprcial_requests')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'track_day7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'more_information')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
