<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\School;

/**
 * SchoolSearch represents the model behind the search form of `app\models\School`.
 */
class SchoolSearch extends School
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id', 'telephone', 'manager_tel', 'administrative_manager_tel', 'Accounts_manager_tel'], 'integer'],
            [['name', 'district', 'city', 'educational_stream', 'manager_name', 'manager_email', 'address', 'administrative_manager_name', 'administrative_manager_email', 'Accounts_manager_name', 'Accounts_manager_email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = School::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'school_id' => $this->school_id,
            'telephone' => $this->telephone,
            'manager_tel' => $this->manager_tel,
            'administrative_manager_tel' => $this->administrative_manager_tel,
            'Accounts_manager_tel' => $this->Accounts_manager_tel,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'educational_stream', $this->educational_stream])
            ->andFilterWhere(['like', 'manager_name', $this->manager_name])
            ->andFilterWhere(['like', 'manager_email', $this->manager_email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'administrative_manager_name', $this->administrative_manager_name])
            ->andFilterWhere(['like', 'administrative_manager_email', $this->administrative_manager_email])
            ->andFilterWhere(['like', 'Accounts_manager_name', $this->Accounts_manager_name])
            ->andFilterWhere(['like', 'Accounts_manager_email', $this->Accounts_manager_email]);

        return $dataProvider;
    }
}
