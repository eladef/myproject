<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HumanResources;

/**
 * HumanResourcesSearch represents the model behind the search form of `app\models\HumanResources`.
 */
class HumanResourcesSearch extends HumanResources
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journey_id'], 'integer'],
            [['educational_coordinator', 'logistics_coordinator', 'guieds', 'operational_coordinator'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HumanResources::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'journey_id' => $this->journey_id,
        ]);

        $query->andFilterWhere(['like', 'educational_coordinator', $this->educational_coordinator])
            ->andFilterWhere(['like', 'logistics_coordinator', $this->logistics_coordinator])
            ->andFilterWhere(['like', 'guieds', $this->guieds])
            ->andFilterWhere(['like', 'operational_coordinator', $this->operational_coordinator]);

        return $dataProvider;
    }
}
