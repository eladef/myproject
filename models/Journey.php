<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "journey".
 *
 * @property int $journey_id
 * @property int $school_id
 * @property string $start_date
 * @property string $final_date
 * @property int $number_of_student_boys
 * @property int $number_of_student_girls
 * @property int $number_of_teachers
 * @property string $layout
 * @property string $mixing_classes
 * @property string $preparation_day
 * @property string $summary_day
 * @property string $journey_manager_name
 * @property int $journey_manager_tel
 * @property string $journey_manager_email
 * @property int $num_of_groups
 */
class Journey extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journey';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id', 'number_of_student_boys', 'number_of_student_girls', 'number_of_teachers', 'journey_manager_tel', 'num_of_groups'], 'integer'],
            [['start_date', 'final_date', 'preparation_day', 'summary_day'], 'safe'],
            [['layout', 'mixing_classes', 'journey_manager_name', 'journey_manager_email'], 'string', 'max' => 255],
        ];
    }


        

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'journey_id' => 'Journey ID',
            'school_id' => 'School name',
            'start_date' => 'Start Date',
            'final_date' => 'Final Date',
            'number_of_student_boys' => 'Number Of Student Boys',
            'number_of_student_girls' => 'Number Of Student Girls',
            'number_of_teachers' => 'Number Of Teachers',
            'layout' => 'Layout',
            'mixing_classes' => 'Mixing Classes',
            'preparation_day' => 'Preparation Day',
            'summary_day' => 'Summary Day',
            'journey_manager_name' => 'Journey Manager Name',
            'journey_manager_tel' => 'Journey Manager Tel',
            'journey_manager_email' => 'Journey Manager Email',
            'num_of_groups' => 'Num Of Groups',
        ];
    }

    public function getSchool(){
        return $this->hasOne(School::className(),['school_id'=>'school_id']);
    }

}
