<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operation".
 *
 * @property int $journey_id
 * @property string $shirts
 * @property string $food_sensitivities
 * @property string $sleeping1
 * @property string $suppliers1
 * @property string $sleeping2
 * @property string $suppliers2
 * @property string $sleeping3
 * @property string $suppliers3
 * @property string $sleeping4
 * @property string $suppliers4
 * @property string $sleeping5
 * @property string $suppliers5
 * @property string $sleeping6
 * @property string $suppliers6
 * @property string $sleeping7
 * @property string $suppliers7
 */
class Operation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shirts', 'food_sensitivities', 'sleeping1', 'suppliers1', 'sleeping2', 'suppliers2', 'sleeping3', 'suppliers3', 'sleeping4', 'suppliers4', 'sleeping5', 'suppliers5', 'sleeping6', 'suppliers6', 'sleeping7', 'suppliers7'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'journey_id' => 'Journey ID',
            'shirts' => 'Shirts',
            'food_sensitivities' => 'Food Sensitivities',
            'sleeping1' => 'Sleeping1',
            'suppliers1' => 'Suppliers1',
            'sleeping2' => 'Sleeping2',
            'suppliers2' => 'Suppliers2',
            'sleeping3' => 'Sleeping3',
            'suppliers3' => 'Suppliers3',
            'sleeping4' => 'Sleeping4',
            'suppliers4' => 'Suppliers4',
            'sleeping5' => 'Sleeping5',
            'suppliers5' => 'Suppliers5',
            'sleeping6' => 'Sleeping6',
            'suppliers6' => 'Suppliers6',
            'sleeping7' => 'Sleeping7',
            'suppliers7' => 'Suppliers7',
        ];
    }
}
