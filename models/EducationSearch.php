<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Education;

/**
 * EducationSearch represents the model behind the search form of `app\models\Education`.
 */
class EducationSearch extends Education
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journey_id'], 'integer'],
            [['sprcial_requests', 'track_day1', 'track_day2', 'track_day3', 'track_day4', 'track_day5', 'track_day6', 'track_day7', 'more_information'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Education::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'journey_id' => $this->journey_id,
        ]);

        $query->andFilterWhere(['like', 'sprcial_requests', $this->sprcial_requests])
            ->andFilterWhere(['like', 'track_day1', $this->track_day1])
            ->andFilterWhere(['like', 'track_day2', $this->track_day2])
            ->andFilterWhere(['like', 'track_day3', $this->track_day3])
            ->andFilterWhere(['like', 'track_day4', $this->track_day4])
            ->andFilterWhere(['like', 'track_day5', $this->track_day5])
            ->andFilterWhere(['like', 'track_day6', $this->track_day6])
            ->andFilterWhere(['like', 'track_day7', $this->track_day7])
            ->andFilterWhere(['like', 'more_information', $this->more_information]);

        return $dataProvider;
    }
}
