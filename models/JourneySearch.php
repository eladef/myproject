<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Journey;

/**
 * JourneySearch represents the model behind the search form of `app\models\Journey`.
 */
class JourneySearch extends Journey
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journey_id', 'school_id', 'number_of_student_boys', 'number_of_student_girls', 'number_of_teachers', 'journey_manager_tel', 'num_of_groups'], 'integer'],
            [['start_date', 'final_date', 'layout', 'mixing_classes', 'preparation_day', 'summary_day', 'journey_manager_name', 'journey_manager_email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Journey::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'journey_id' => $this->journey_id,
            'school_id' => $this->school_id,
            'start_date' => $this->start_date,
            'final_date' => $this->final_date,
            'number_of_student_boys' => $this->number_of_student_boys,
            'number_of_student_girls' => $this->number_of_student_girls,
            'number_of_teachers' => $this->number_of_teachers,
            'preparation_day' => $this->preparation_day,
            'summary_day' => $this->summary_day,
            'journey_manager_tel' => $this->journey_manager_tel,
            'num_of_groups' => $this->num_of_groups,
        ]);

        $query->andFilterWhere(['like', 'layout', $this->layout])
            ->andFilterWhere(['like', 'mixing_classes', $this->mixing_classes])
            ->andFilterWhere(['like', 'journey_manager_name', $this->journey_manager_name])
            ->andFilterWhere(['like', 'journey_manager_email', $this->journey_manager_email]);

        return $dataProvider;
    }
}
