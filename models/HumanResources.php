<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "human_resources".
 *
 * @property int $journey_id
 * @property string $educational_coordinator
 * @property string $logistics_coordinator
 * @property string $guieds
 * @property string $operational_coordinator
 */
class HumanResources extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'human_resources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['educational_coordinator', 'logistics_coordinator', 'guieds', 'operational_coordinator'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'journey_id' => 'Journey ID',
            'educational_coordinator' => 'Educational Coordinator',
            'logistics_coordinator' => 'Logistics Coordinator',
            'guieds' => 'Guieds',
            'operational_coordinator' => 'Operational Coordinator',
        ];
    }
}
