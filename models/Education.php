<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property int $journey_id
 * @property string $sprcial_requests
 * @property string $track_day1
 * @property string $track_day2
 * @property string $track_day3
 * @property string $track_day4
 * @property string $track_day5
 * @property string $track_day6
 * @property string $track_day7
 * @property string $more_information
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sprcial_requests', 'track_day1', 'track_day2', 'track_day3', 'track_day4', 'track_day5', 'track_day6', 'track_day7', 'more_information'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'journey_id' => 'Journey ID',
            'sprcial_requests' => 'Sprcial Requests',
            'track_day1' => 'Track Day1',
            'track_day2' => 'Track Day2',
            'track_day3' => 'Track Day3',
            'track_day4' => 'Track Day4',
            'track_day5' => 'Track Day5',
            'track_day6' => 'Track Day6',
            'track_day7' => 'Track Day7',
            'more_information' => 'More Information',
        ];
    }
}
