<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operation;

/**
 * OperationSearch represents the model behind the search form of `app\models\Operation`.
 */
class OperationSearch extends Operation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journey_id'], 'integer'],
            [['shirts', 'food_sensitivities', 'sleeping1', 'suppliers1', 'sleeping2', 'suppliers2', 'sleeping3', 'suppliers3', 'sleeping4', 'suppliers4', 'sleeping5', 'suppliers5', 'sleeping6', 'suppliers6', 'sleeping7', 'suppliers7'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'journey_id' => $this->journey_id,
        ]);

        $query->andFilterWhere(['like', 'shirts', $this->shirts])
            ->andFilterWhere(['like', 'food_sensitivities', $this->food_sensitivities])
            ->andFilterWhere(['like', 'sleeping1', $this->sleeping1])
            ->andFilterWhere(['like', 'suppliers1', $this->suppliers1])
            ->andFilterWhere(['like', 'sleeping2', $this->sleeping2])
            ->andFilterWhere(['like', 'suppliers2', $this->suppliers2])
            ->andFilterWhere(['like', 'sleeping3', $this->sleeping3])
            ->andFilterWhere(['like', 'suppliers3', $this->suppliers3])
            ->andFilterWhere(['like', 'sleeping4', $this->sleeping4])
            ->andFilterWhere(['like', 'suppliers4', $this->suppliers4])
            ->andFilterWhere(['like', 'sleeping5', $this->sleeping5])
            ->andFilterWhere(['like', 'suppliers5', $this->suppliers5])
            ->andFilterWhere(['like', 'sleeping6', $this->sleeping6])
            ->andFilterWhere(['like', 'suppliers6', $this->suppliers6])
            ->andFilterWhere(['like', 'sleeping7', $this->sleeping7])
            ->andFilterWhere(['like', 'suppliers7', $this->suppliers7]);

        return $dataProvider;
    }
}
