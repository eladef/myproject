<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school".
 *
 * @property int $school_id
 * @property string $name
 * @property string $district
 * @property string $city
 * @property int $telephone
 * @property string $educational_stream
 * @property string $manager_name
 * @property int $manager_tel
 * @property string $manager_email
 * @property string $address
 * @property string $administrative_manager_name
 * @property int $administrative_manager_tel
 * @property string $administrative_manager_email
 * @property string $Accounts_manager_name
 * @property int $Accounts_manager_tel
 * @property string $Accounts_manager_email
 */
class School extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telephone', 'manager_tel', 'administrative_manager_tel', 'Accounts_manager_tel'], 'integer'],
            [['name', 'district', 'city', 'educational_stream', 'manager_name', 'manager_email', 'address', 'administrative_manager_name', 'administrative_manager_email', 'Accounts_manager_name', 'Accounts_manager_email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'school_id' => 'School ID',
            'name' => 'Name',
            'district' => 'District',
            'city' => 'City',
            'telephone' => 'Telephone',
            'educational_stream' => 'Educational Stream',
            'manager_name' => 'Manager Name',
            'manager_tel' => 'Manager Tel',
            'manager_email' => 'Manager Email',
            'address' => 'Address',
            'administrative_manager_name' => 'Administrative Manager Name',
            'administrative_manager_tel' => 'Administrative Manager Tel',
            'administrative_manager_email' => 'Administrative Manager Email',
            'Accounts_manager_name' => 'Accounts Manager Name',
            'Accounts_manager_tel' => 'Accounts Manager Tel',
            'Accounts_manager_email' => 'Accounts Manager Email',
        ];
    }
}
